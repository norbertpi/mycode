## A dictionary linking a room to other rooms                                                                                                                  
rooms = {                                                                                                                                                      
                                                                                                                                                               
            'Hall' : {                                                                                                                                         
                  'south' : 'Kitchen',                                                                                                                         
                  'east'  : 'Dining Room',                                                                                                                     
                  'west'  : 'Workbench',                                                                                                                       
                  'item'  : 'key'                                                                                                                              
                },                                                                                                                                             
                                                                                                                                                               
            'Kitchen' : {                                                                                                                                      
                  'north' : 'Hall',                                                                                                                            
                  'item'  : 'monster',                                                                                                                         
                },                                                                                                                                             
            'Dining Room' : {                                                                                                                                  
                  'west' : 'Hall',                                                                                                                             
                  'south': 'Garden',                                                                                                                           
                  'item' : 'potion'                                                                                                                            
               },                                                                                                                                              
            'Garden' : {                                                                                                                                       
                  'north' : 'Dining Room'                                                                                                                      
            },                                                                                                                                                 
            'Workbench' : {                                                                                                                                    
                   'east' : 'Kitchen'                                                                                                                          
            }                                                                                                                                                  
         }
