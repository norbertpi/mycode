#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests


eoss = 'http://api.open-notify.org/iss-pass.json?lat=47.6&lon=-122.3'

def main():
    """your code goes below here"""
   
    ## Call the webserv
    trackiss = urllib.request.urlopen(eoss)

    # stuck? you can always write comments
    # Try describe the steps you would take manually



if __name__ == "__main__":
    main()

